import java.util.InputMismatchException;

/**
 Escribe un programa , utilizando para ello el paradigma de POO, que lance y
capture una excepción customizada. Crea para ello una package diferente que
puedas reutilizar para el resto de tus proyectos.
Recomendaciones:
El programa abre un bucle try{} en el que comienza mostrando un mensaje
por pantalla. A continuación, crea un objeto de la clase Exception, indicando
en su constructor un mensaje explicativo. 
 */

/**
 * @author francis
 *
 */
import Excepciones.ExcepcionManager;

public class T10_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			System.out.println("Mostrando excepción:");
			int a = 5 / 0;
		} catch (Exception exc){
			ExcepcionManager em = new ExcepcionManager("Error 5 / 0");
			String res = em.Tratar();
			System.out.println(res);
		} finally {
			System.out.println("Proceso finalizado:");
		}
	}

}
