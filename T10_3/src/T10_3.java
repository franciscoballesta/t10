/**
Escribe un programa , utilizando para ello el paradigma de POO, que genere
un n�mero aleatorio e indique si el n�mero generado es par o impar. El
programa utilizar� para ello el lanzamiento de una excepci�n.
Recomendaciones:
1. El programa utiliza la clase Random() para obtener un n�mero aleatorio
entre 0 y 999 (por poner un rango cualquiera).
2. Se determina si el n�mero es par o impar y se lanza una excepci�n con el
correspondiente mensaje para indicarlo (se limitar� a mostrar el mensaje
asociado a la excepci�n capturada)
 */

/**
 * @author francis
 *
 */
public class T10_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ParImpar p = null;
		try {
			System.out.println("Generando n�mero aleatorio:");
			p = new ParImpar(1, 300);
			System.out.println("Valor: " + p.valor);
			p.evaluar();
		} catch (Exception exc){
			System.out.println(exc.getMessage());
		}

	}

}
