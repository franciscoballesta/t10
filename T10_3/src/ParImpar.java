/**
 * 
 */

/**
 * @author francis
 *
 */
public class ParImpar {
	public int valor;
    public ParImpar(int minimo, int maximo)
    {
    	valor = Aleatorio(minimo, maximo);
    }
    public String evaluar() throws Exception {
        if(valor % 2 == 0) {
   	     	throw new Exception("El n�mero es par.");
        }else {
        	throw new Exception("El n�mero es impar.");
        }
    }
    private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}

}
