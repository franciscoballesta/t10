
public class Ambito {
	private int minimo;
	private int maximo;
	private int valor;
	public boolean correcto = false;
	private int intentos = 0;
	public int getIntentos()
    {
          return intentos;
    }
    public void setIntentos(int intento)
    {
        intentos += intento;
    }
    public void setValor(int valor) throws Exception
    {
        intentos ++;
         if(this.valor == valor) { 
        	 correcto = true;
        	 return;
         }
         if(valor<minimo || valor>maximo)
        	 throw new Exception("Incorrecto, fuera de rango.");
         if(valor<this.valor) {
        	 minimo=valor;
    	     throw new Exception("El n�mero es mayor.");
         }
         if(valor>this.valor) {
        	 maximo=valor;	
        	 throw new Exception("El n�mero es menor.");
         }
         
    }
	public int getValor()
    {
          return valor;
    }
    public Ambito(int minimo, int maximo)
    {
    	this.minimo=minimo;
    	this.maximo=maximo;
    	valor = Aleatorio(minimo, maximo);
    	correcto = false;
    	
    }
	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}
}
