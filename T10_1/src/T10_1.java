import java.util.InputMismatchException;
import java.util.Scanner;

/**
Escribe un programa, utilizando para ello el paradigma de POO, que juegue
con el usuario a adivinar un n�mero. Debe cumplir los siguientes
requerimientos:
1. El ordenador debe generar un n�mero entre 1 y 500, y el usuario tiene
que intentar adivinarlo.
2. Cada vez que el usuario introduce un valor, el ordenador debe decirle al
usuario si el n�mero que tiene que adivinar es mayor o menor que el que
ha introducido el usuario.
3. Cuando consiga adivinarlo, debe indic�rselo e imprimir en pantalla el
n�mero de veces que el usuario ha intentado adivinar el n�mero.
4. Si el usuario introduce algo que no es un n�mero, debe indicarlo en
pantalla, y contarlo como un intento indicando que no ha conseguido
reconocer la entrada lanzando la excepci�n InputMismatchException.
 */

/**
 * @author francis
 *
 */
public class T10_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Introduce n�mero a buscar. Cero para salir.");			
		Ambito ambito = new Ambito(1,500);
		int numero = 0;
		do {
		  try {
		    numero = reader.nextInt();
		    if(numero!=0) {
		    	ambito.setValor(numero);
		    	if(ambito.correcto)
		    	numero = 0;
		    }
		  } catch (InputMismatchException exc){
		    System.out.println("Error: Solo n�meros. ");
		    ambito.setIntentos(1);
		  } catch (Exception exc){
			System.out.println(exc.getMessage());
		  }			
		} while (numero!=0);
		reader.close();
		
		if(ambito.correcto) 
			System.out.println("N�mero encontrado! => " + ambito.getValor());
		System.out.println("N�mero de intentos => " + ambito.getIntentos());
		
	}

}
