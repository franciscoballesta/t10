
public class Calculadora {
	private int operador1;
	private int operador2;
	private String operacion;
	private double resultado;
	public Calculadora() {
		
	}
	public void setOperador1(int Op) {
		operador1=Op;
	}
	public void setOperador2(int Op) {
		operador2=Op;
	}
	public void setOperacion(String StrOp) {
		operacion=StrOp;
	}
	public String getOperaciones() {
		return "+(suma), -(resta), *(mult), /(div), r(raiz)";
	}
	public void Resolver() {
			resultado = 0;
			switch (operacion) {
			case "+":
				resultado = operador1 + operador2;
				break;
			case "-":
				resultado = operador1 - operador2;
				break;
			case "*":
				resultado = operador1 * operador2;
				break;
			case "/":
				if(operador2==0) throw new ArithmeticException("Divisi�n por cero");
				resultado = operador1 / operador2;
				break;
			case "r":
				if(operador2==0) throw new ArithmeticException("Cero no permitido");
				resultado = Math.pow(operador1, (1/operador2));
				break;
			default:
				break;
			}
	}
	public double getResultado() {
		return (double)resultado;
	}
}
