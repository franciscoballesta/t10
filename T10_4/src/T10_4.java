import java.util.InputMismatchException;
import java.util.Scanner;

/**
Escribe un programa , utilizando para ello el paradigma de POO, que nos
permita realizar c�lculos simples (suma, resta, multiplicaci�n, potencia, ra�z
cuadrada, ra�z cubica y divisi�n). El programa ha des estar preparado para
gestionar los posibles errores de calculo. Has de utilizar para ello el control de
excepciones de JAVA.
Recomendaciones:
1. Utiliza siempre que sea posible las Excepciones definidas en la API de Java
8.
2. Puedes utilizar como interfaz visual Scanner o JOptionPane.
3. Estructura correctamente el c�digo en diferentes packages.
 */

/**
 * @author francis
 *
 */
import Excepciones.ExcepcionManager;
public class T10_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Calculadora calc = new Calculadora();
		double resultado = 0;
		ExcepcionManager em = new ExcepcionManager();
		int numero = 0;
		do {
		  try {
			System.out.println("Introduce primer operador. Cero para salir.");			
		    numero = reader.nextInt();
		    if(numero!=0) {
		    	calc.setOperador1(numero);
		    	String Op = "";
				System.out.println("Introduce operaci�n: " + calc.getOperaciones());			
		    	Op = reader.next();
		    	calc.setOperacion(Op);
				System.out.println("Introduce segundo operador.");			
			    numero = reader.nextInt();
		    	calc.setOperador2(numero);
		    	calc.Resolver();
		    	resultado = calc.getResultado();
		    }
		  } catch (InputMismatchException exc){
		    System.out.println("Error: Solo n�meros. ");
		  } catch (ArithmeticException ae) {
			  System.out.println(em.Tratar(ae.getMessage()));
			resultado = 0;
		  } catch (Exception e) {
			  System.out.println(em.Tratar(e.getMessage()));
			resultado=0;
		  }
		  System.out.println("Resultado: " + resultado);
		} while (numero!=0);
		reader.close();

	}

}
